import Vue from 'vue';
import Router from 'vue-router';
import ProductDetails from '@/pdp/components/ProductDetails';
import Header from '@/pdp/components/Header';
import Footer from '@/pdp/components/Footer';
import 'bootstrap/dist/css/bootstrap.css';
import Vuex from 'vuex';

Vue.use(Vuex);
Vue.use(Router);

// in your code
export default new Router({
  routes: [
    {
      path: '/',
      components: {
        default: ProductDetails,
        header: Header,
        footer: Footer
      }
    }
  ]
});
