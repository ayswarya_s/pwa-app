import Vue from 'vue';
import SpinnerVue from './Spinner';

export default {
  install(Vue, defaultOptions = {}) {
    const CONSTRUCTOR = Vue.extend(SpinnerVue);
    const CACHE = {};
    Object.assign(SpinnerVue.DEFAULT_OPT, defaultOptions);

    function showSpinner() {

    }

    function hideSpinner() {

    }
    Vue.showSpinner = Vue.prototype.$showSpinner = showSpinner;
    Vue.hideSpinner = Vue.prototype.$hideSpinner = hideSpinner;
  }
}
