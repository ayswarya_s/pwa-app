import Vue from 'vue';
// register
Vue.component('my-component', {
  template: '<div>A custom component!</div>'
});
/* eslint-disable no-new */
new Vue({
  el: '#example'
});
