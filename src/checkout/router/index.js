import Vue from 'vue';
import Router from 'vue-router';
import Taxonomy from '@/checkout/components/Taxonomy';
import Header from '@/checkout/components/Header';
import Footer from '@/checkout/components/Footer';
import ProductList from '@/checkout/components/ProductList';
import Checkout from '@/checkout/components/Checkout';
import 'bootstrap/dist/css/bootstrap.css';
import Vuex from 'vuex';
// import Toast from '../plugins/toast';

Vue.use(Vuex);
Vue.use(Router);
// Vue.use(Toast);

// in your code
export default new Router({
  routes: [
    {
      path: '/',
      components: {
        default: Taxonomy,
        header: Header,
        footer: Footer,
        productList: ProductList
      }
    },
    {
      path: '/checkout',
      components: {
        default: Checkout,
        header: Header,
        footer: Footer
      }
    }
  ]
});
