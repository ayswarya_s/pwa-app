import * as types from './mutation-types'

export const addToCart = ({ commit }, sku) => {
  commit(types.ADD_TO_CART, {
    sku
  })
}

export const getAllProducts = ({ commit }) => {
  commit(types.REQUEST_PRODUCTS);
}

export const requestProductDetails = ({ commit }) => {
  commit(types.REQUEST_PRODUCT_DETAILS);
}

export const getTaxonomy = ({ commit }) => {
  commit(types.REQUEST_TAXONOMY);
}

export const getSubcategories = ({ commit }) => {
  commit(types.REQUEST_SUBCATEGORY);
}
