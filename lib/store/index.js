import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import * as actions from './actions'
import * as getters from './getters'
import cart from './modules/cart'
import products from './modules/products'
import catalog from './modules/catalog'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Store({
  actions,
  getters,
  modules: {
    cart,
    products,
    catalog
  },
  strict: debug
})
