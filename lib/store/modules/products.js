import shop from '../../api/shop'
import * as types from '../mutation-types'
import _ from 'lodash';
// initial state
const state = {
  all: [],
  productDetails: {}
}

// getters
const getters = {
  allProducts: state => state.all,
  productDetails: state => state.productDetails
}

// actions
const actions = {
  getAllProducts ({ commit }, args) {
    shop.getProducts(args[0], args[1], (products) => {
      commit(types.RECEIVE_PRODUCTS, { products });
    });
  },

  requestProductDetails ({ commit }, args) {
    shop.getProductDetails(args[0], (detail) => {
      commit(types.RECEIVE_PRODUCT_DETAILS, { detail });
    });
  }
}

// mutations
const mutations = {
  [types.RECEIVE_PRODUCTS] (state, { products }) {
    const response = [];
    _.each(products, (product) => {
      response.push(product._source);
    })
    state.all = response
  },

  [types.REQUEST_PRODUCTS] () {

  },

  [types.REQUEST_PRODUCT_DETAILS] () {

  },

  [types.RECEIVE_PRODUCT_DETAILS] (state, { detail }) {
    state.productDetails = detail;
  },

  [types.ADD_TO_CART] (state, { id }) {
    state.all.find(p => p.id === id).inventory--
  }
}
export default {
  state,
  getters,
  actions,
  mutations
}
