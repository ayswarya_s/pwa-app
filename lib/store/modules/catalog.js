import shop from '../../api/shop';
import * as types from '../mutation-types';
import _ from 'lodash';

const state = {
  all: []
}

const getters = {
  productFamilies: state => state.all,
  productSubCategories: (state) => (category) => {
    const subCategories = [];
    _.each(state.all, (eachCategory) => {
      if (category.includes(eachCategory.path)) {
        subCategories.push(eachCategory.product_categories);
      }
    });
    return subCategories;
  }
}

const actions = {
  getTaxonomy ({ commit }) {
    shop.getTaxonomy(taxonomy => {
      commit(types.RECEIVE_TAXONOMY, { taxonomy })
    });
  }

}

const mutations = {
  [types.RECEIVE_TAXONOMY] (state, { taxonomy }) {
    state.all = taxonomy;
  },

  [types.REQUEST_TAXONOMY] () {

  },

  [types.REQUEST_SUBCATEGORY] () {

  },

  [types.REQUEST_PRODUCT_DETAILS] () {

  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
