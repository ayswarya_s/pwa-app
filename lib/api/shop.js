/**
 * Mocking client-server processing
 */
'use strict';
import { ServiceClients } from 'service-client-lib';

export default {
  getProducts: (category, subCategory, cb) => {
    const { CatalogClient } = ServiceClients;
    const catalogClient = new CatalogClient();
    const searchData = {
      "query": {
        "bool": {
          "must": [{
            "match": {
              "taxonomy.product_family": category.toLowerCase()
            }
          },{
            "match": {
              "taxonomy.product_category": subCategory.toLowerCase()
            }
          }]
        }
      }
    }
    catalogClient.getProducts(searchData)
      .then((data) => {
        cb(data.hits.hits);
      })
      .catch((err) => {
        throw err;
      });
  },

  getTaxonomy: cb => {
    const { CatalogClient } = ServiceClients;
    const catalogClient = new CatalogClient();
    catalogClient.getTaxonomy()
      .then((data) => {
        cb(data[0].product_families);
      })
      .catch((err) => {
        throw err;
      });
  },

  getProductDetails: (id, cb) => {
    const { CatalogClient } = ServiceClients;
    const catalogClient = new CatalogClient;
    catalogClient.getProductDetails(id)
      .then((data) => {
        cb(data[0]);
      })
      .catch((err) => {
        throw err;
      });
  }
}
