self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open('pwa-app').then(function(cache) {
      return cache.addAll([
        '/',
        '/static/fonts/glyphicons-halflings-regular.448c34a.woff2'
      ])
    });
  )
});
