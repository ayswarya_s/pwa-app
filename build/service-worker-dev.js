if ('serviceWorker' in navigator) {
  window.addEventListener('load', function() {
    navigator.serviceWorker.register('service-worker.js')
    .then(function(registration) {
      console.log('registration successful', registration.scope);
    })
    .catch(function(err) {
      console.log('service worker registration failed', err);
    });

  });
}
