require('./check-versions')()

var config = require('../config')
if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = JSON.parse(config.dev.env.NODE_ENV)
}
var _ = require('lodash');
var opn = require('opn')
var path = require('path')
var hapi = require('hapi')
var webpack = require('hapi-webpack-plugin')
var inert = require('inert');
var proxyMiddleware = require('http-proxy-middleware')
var webpackConfig = process.env.NODE_ENV === 'testing'
  ? require('./webpack.prod.conf')
  : require('./webpack.dev.conf')

// default port where dev server listens for incoming traffic
var port = process.env.PORT || config.dev.port

var proxyTable = config.dev.proxyTable
const Vue = require('vue')
const app = new Vue({
  template: `<div>Hello World</div>`
})

// const renderer = require('vue-server-renderer').createRenderer(require('fs').readFileSync('./dist/index.html', 'utf-8'));

var server = new hapi.Server({
  connections: {
    routes: {
      files: {
        relativeTo: path.join(__dirname, '..','dist')
      }
    }
  }
});

let publicConnection = server.connection({
  port: port,
  routes: {
    cors: true
  },
  labels: ['public']
});

server.register(inert, () => {});
server.register([
  {
    register: webpack,
    options: './build/webpack.dev.conf.js'
  },
  require('vision'),
  require('inert')
], function(err) {
    if(err) {
      console.log(err);
      throw err;
    }

  server.route({
    method: 'GET',
    path: '/service-worker.js',
    handler: function(request, reply) {
      return reply.file("../dist/worker-registration.js");
    }
  });
  //
  // server.route({
  //   method: 'GET',
  //   path: '/{path*}',
  //   handler: function(request, reply) {
  //     // const app = new Vue({
  //     //   data: {
  //     //     url: request.url.path
  //     //   },
  //     //   template: "<div>The visited URL is: {{ url }}</div>"
  //     // });
  //     renderer.renderToString(app, (err, html) => {
  //       console.log(html);
  //       // if (err) {
  //       //   return reply('Internal Server Error').code(500);
  //       // }
  //       // console.log(html);
  //       // console.log("-----------");
  //       // return reply(`<!DOCTYPE html><html lang="en"><head><title>Hello</title></head><body></body></html>`)
  //     });
  //
  //   }
  // });


  var uri = 'http://localhost:' + port

  try {
    server.start(function() {
      _.forEach(server.connections, function(connection) {
        console.log(connection.info)
        console.log('Server started at: ' + connection.info.uri);
      });
    });
  } catch (err) {
    console.log(err);
  }
});
